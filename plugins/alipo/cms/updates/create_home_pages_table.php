<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHomePagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_home_pages')){
            Schema::create('alipo_cms_home_pages', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('background');
                $table->timestamps();
            });
    

        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_home_pages');
    }
}
