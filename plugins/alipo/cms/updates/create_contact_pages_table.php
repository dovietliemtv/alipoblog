<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContactPagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_contact_pages')){
            Schema::create('alipo_cms_contact_pages', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('contact_info');
                $table->text('form_text');
                $table->timestamps();
            });
    
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_contact_pages');
    }
}
