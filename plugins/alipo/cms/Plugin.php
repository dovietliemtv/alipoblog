<?php namespace Alipo\Cms;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Cms Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Cms',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Cms', 'cms', 'plugins/alipo/cms/partials/sidebar');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {

        return [
            'Alipo\Cms\Components\HomePageCp' => 'homepagecp',
            'Alipo\Cms\Components\ContactPageCp' => 'contactpagecp',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'alipo.cms.manager_seo' => [
                'tab' => 'Cms',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'cms' => [
                'label'       => 'Content',
                'url'         => Backend::url('alipo/cms/contactpage/update/1'),
                'icon'        => 'icon-leaf',
                'permissions' => ['alipo.cms.*'],
                'order'       => 500,
                'sideMenu' => [
                    'contactpage' => [
                        'label'       => 'Contact',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/cms/contactpage/update/1'),
                        'permissions' => ['alipo.cms.*'],
                        'group'       => 'Pages'
                    ],
                 
                ]
            ],
        ];
    }
}
