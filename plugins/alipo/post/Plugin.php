<?php namespace Alipo\Post;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Post Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Post',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Post', 'post', 'plugins/alipo/post/partials/sidebar');

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Alipo\Post\Components\PostCp' => 'postcp',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        // return []; // Remove this line to activate

        return [
            'alipo.post.*' => [
                'tab' => 'Post',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'post' => [
                'label'       => 'Blog',
                'url'         => Backend::url('alipo/post/post'),
                'icon'        => 'icon-medium',
                'permissions' => ['alipo.post.*'],
                'order'       => 500,
                'sideMenu' => [
                    'post' => [
                        'label'       => 'News',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/post/post'),
                        'permissions' => ['alipo.post.news'],
                        'group'       => 'Posts'
                    ],
                    'category' => [
                        'label'       => 'Category',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/post/category'),
                        'permissions' => ['alipo.post.category'],
                        'group'       => 'Category'
                    ],
                ]
            ],
        ];
    }
}
