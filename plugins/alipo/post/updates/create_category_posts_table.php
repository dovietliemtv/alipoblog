<?php namespace Alipo\Post\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCategoryPostsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_post_category_posts')){ 
            Schema::create('alipo_post_category_posts', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('category_id')->unsigned();
                $table->integer('post_id')->unsigned();
                $table->primary(['post_id', 'category_id']);
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_post_category_posts');
    }
}
