<?php namespace Alipo\Post\Models;

use Model;

/**
 * Post Model
 */
class Post extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_post_posts';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $rules = [
        'title'     => 'required',
        'slug'  => 'required',
        'thumbnail'     => 'required',
        'description'     => 'required',
    ];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'categories' => [
            'Alipo\Post\Models\Category',
            'table' => 'alipo_post_category_posts',
            'key'      => 'post_id'
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'thumbnail' => 'System\Models\File'
    ];
    public $attachMany = [];
}
