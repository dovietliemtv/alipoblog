<?php namespace Alipo\Post\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Post Back-end Controller
 */
class Post extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Alipo.Post', 'post', 'post');
    }
    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'thumbnail' ) {
            return '<img src="' . $record->thumbnail->path . '" width="75"/>';
        }
    }

}
